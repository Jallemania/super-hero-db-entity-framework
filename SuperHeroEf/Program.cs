using Microsoft.EntityFrameworkCore;
using PostGradEF.DataAccess;
using PostGradEF.DataAccess.Interfaces;
using PostGradEF.Services;
using PostGradEF.Services.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<ISuperHeroRepository, SuperHeroRepository>();
builder.Services.AddScoped<ISuperHeroService, SuperHeroService>();

builder.Services.AddScoped<ISuperPowerRepository, SuperPowerRepository>();
builder.Services.AddScoped<ISuperPowerService, SuperPowerService>();

builder.Services.AddDbContext<SuperheroDbContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
