﻿using PostGradEF.DataAccess.Interfaces;
using PostGradEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradEF.DataAccess
{
    public class SuperPowerRepository : ISuperPowerRepository
    {
        public SuperPower Create(SuperPower superPower)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<SuperPower> Read()
        {
            throw new NotImplementedException();
        }

        public SuperPower Read(int id)
        {
            throw new NotImplementedException();
        }

        public SuperPower Read(string name)
        {
            throw new NotImplementedException();
        }

        public SuperPower Update(SuperPower editedSuperPower)
        {
            throw new NotImplementedException();
        }
    }
}
