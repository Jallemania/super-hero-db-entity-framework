﻿using PostGradEF.DataAccess.Interfaces;
using PostGradEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradEF.DataAccess
{
    public class SuperHeroRepository : ISuperHeroRepository
    {
        private readonly SuperheroDbContext _context;

        public SuperHeroRepository(SuperheroDbContext context)
        {
            _context = context;
        }


        public SuperHero Create(SuperHero professor)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<SuperHero>? Read()
        {
            return _context.SuperHeroes?.ToList();
        }

        public SuperHero Read(int id)
        {
            throw new NotImplementedException();
        }

        public SuperHero Read(string name)
        {
            throw new NotImplementedException();
        }

        public SuperHero Update(SuperHero editedProfessor)
        {
            throw new NotImplementedException();
        }
    }
}
