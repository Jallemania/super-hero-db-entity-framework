﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using PostGradEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace PostGradEF.DataAccess
{
    public class SuperheroDbContext : DbContext
    {
        public DbSet<SuperHero> ?SuperHeroes { get; set; }
        public DbSet<SideKick> ?SideKicks { get; set; }
        public DbSet<SuperPower> ?SuperPowers { get; set; }

        public SuperheroDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<SuperHero>()
                .HasMany(p => p.Powers)
                .WithMany(m => m.Heroes)
                .UsingEntity<Dictionary<string, object>>(
                    "HeroPowers",
                    r => r.HasOne<SuperPower>().WithMany().HasForeignKey("PowerId"),
                    l => l.HasOne<SuperHero>().WithMany().HasForeignKey("HeroId"),
                    je =>
                    {
                        je.HasKey("HeroId", "PowerId");
                        // Seed Here
                    });



            SuperHero hero1 = new()
            {
                Id = 1,
                Alias = "Batman",
                Name = "Bruce Wayne",
                Affiliation = "Justice League"
            };
            SuperHero hero2 = new()
            {
                Id= 2,
                Alias = "Superman",
                Name = "Clark Kent",
                Affiliation = "Justice League"
            };

            builder.Entity<SuperHero>()
                .HasData(hero1, hero2);
        }
    }
}
