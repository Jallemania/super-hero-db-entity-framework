﻿using PostGradEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradEF.DataAccess.Interfaces
{
    public interface ISuperHeroRepository
    {
        public SuperHero Create(SuperHero professor);
        public List<SuperHero> Read();
        public SuperHero Read(int id);
        public SuperHero Read(string name);
        public SuperHero Update(SuperHero editedProfessor);
        public bool Delete(int id);

    }
}
