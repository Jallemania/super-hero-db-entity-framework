﻿using PostGradEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradEF.DataAccess.Interfaces
{
    public interface ISuperPowerRepository
    {
        public SuperPower Create(SuperPower superPower);
        public List<SuperPower> Read();
        public SuperPower Read(int id);
        public SuperPower Read(string name);
        public SuperPower Update(SuperPower editedSuperPower);
        public bool Delete(int id);
    }
}
