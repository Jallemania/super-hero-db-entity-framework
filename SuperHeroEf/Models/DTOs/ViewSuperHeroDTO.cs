﻿using PostGradEF.Models;

namespace SuperHeroEf.Models.DTOs
{
    public class ViewSuperHeroDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Alias { get; set; }
        public string? Affiliation { get; set; }
        public virtual ICollection<SuperPower>? Powers { get; set; }

        public ViewSuperHeroDTO(SuperHero hero)
        {
            Id = hero.Id;
            Name = hero.Name;
            Alias = hero.Alias;
            Affiliation = hero.Affiliation;
            Powers = hero.Powers;
        }
    }
}
