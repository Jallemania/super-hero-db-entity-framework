﻿using System;
using System.Collections.Generic;

namespace PostGradEF.Models
{
    public partial class SuperPower
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int? Strength { get; set; }
        public string? Type { get; set; }
        public virtual ICollection<SuperHero> Heroes { get; set; }
    }
}
