﻿using System;
using System.Collections.Generic;

namespace PostGradEF.Models
{
    public class SuperHero
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Alias { get; set; }
        public string? Affiliation { get; set; }
        public virtual ICollection<SuperPower>? Powers { get; set; }
    }
}
