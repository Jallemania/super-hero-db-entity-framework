﻿using System;
using System.Collections.Generic;

namespace PostGradEF.Models
{
    public partial class SideKick
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public SuperHero Hero { get; set; }

    }
}
