﻿using
    Microsoft.AspNetCore.Mvc;
using PostGradEF.DataAccess.Interfaces;
using SuperHeroEf.Models.DTOs;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SuperHeroEf.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SuperHeroController : ControllerBase
    {
        private readonly ISuperHeroRepository _heroRepository;
        public SuperHeroController(ISuperHeroRepository heroRepository)
        {
            _heroRepository = heroRepository;
        }


        // GET: api/<SuperHeroController>
        [HttpGet]
        public ICollection<ViewSuperHeroDTO> Get()
        {
            List<ViewSuperHeroDTO> heroList = new();
            foreach (var hero in _heroRepository.Read())
            {
                heroList.Add(new ViewSuperHeroDTO(hero));
            }
            return heroList;
        }

        // GET api/<SuperHeroController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<SuperHeroController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<SuperHeroController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<SuperHeroController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
