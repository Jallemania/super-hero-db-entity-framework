﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SuperHeroEf.Migrations
{
    public partial class seedheroes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "SuperHeroes",
                columns: new[] { "Id", "Affiliation", "Alias", "Name" },
                values: new object[] { 1, "Justice League", "Batman", "Bruce Wayne" });

            migrationBuilder.InsertData(
                table: "SuperHeroes",
                columns: new[] { "Id", "Affiliation", "Alias", "Name" },
                values: new object[] { 2, "Justice League", "Superman", "Clark Kent" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "SuperHeroes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "SuperHeroes",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
