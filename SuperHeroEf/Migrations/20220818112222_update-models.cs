﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SuperHeroEf.Migrations
{
    public partial class updatemodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Faculty",
                table: "SuperPowers");

            migrationBuilder.RenameColumn(
                name: "Phone",
                table: "SuperPowers",
                newName: "Type");

            migrationBuilder.RenameColumn(
                name: "Office",
                table: "SuperPowers",
                newName: "Description");

            migrationBuilder.RenameColumn(
                name: "Phone",
                table: "SuperHeroes",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "LastName",
                table: "SuperHeroes",
                newName: "Alias");

            migrationBuilder.RenameColumn(
                name: "FirstName",
                table: "SuperHeroes",
                newName: "Affiliation");

            migrationBuilder.AddColumn<int>(
                name: "Strength",
                table: "SuperPowers",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HeroId",
                table: "SideKicks",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "SuperHeroSuperPower",
                columns: table => new
                {
                    HeroesId = table.Column<int>(type: "int", nullable: false),
                    PowersId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SuperHeroSuperPower", x => new { x.HeroesId, x.PowersId });
                    table.ForeignKey(
                        name: "FK_SuperHeroSuperPower_SuperHeroes_HeroesId",
                        column: x => x.HeroesId,
                        principalTable: "SuperHeroes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SuperHeroSuperPower_SuperPowers_PowersId",
                        column: x => x.PowersId,
                        principalTable: "SuperPowers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SideKicks_HeroId",
                table: "SideKicks",
                column: "HeroId");

            migrationBuilder.CreateIndex(
                name: "IX_SuperHeroSuperPower_PowersId",
                table: "SuperHeroSuperPower",
                column: "PowersId");

            migrationBuilder.AddForeignKey(
                name: "FK_SideKicks_SuperHeroes_HeroId",
                table: "SideKicks",
                column: "HeroId",
                principalTable: "SuperHeroes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SideKicks_SuperHeroes_HeroId",
                table: "SideKicks");

            migrationBuilder.DropTable(
                name: "SuperHeroSuperPower");

            migrationBuilder.DropIndex(
                name: "IX_SideKicks_HeroId",
                table: "SideKicks");

            migrationBuilder.DropColumn(
                name: "Strength",
                table: "SuperPowers");

            migrationBuilder.DropColumn(
                name: "HeroId",
                table: "SideKicks");

            migrationBuilder.RenameColumn(
                name: "Type",
                table: "SuperPowers",
                newName: "Phone");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "SuperPowers",
                newName: "Office");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "SuperHeroes",
                newName: "Phone");

            migrationBuilder.RenameColumn(
                name: "Alias",
                table: "SuperHeroes",
                newName: "LastName");

            migrationBuilder.RenameColumn(
                name: "Affiliation",
                table: "SuperHeroes",
                newName: "FirstName");

            migrationBuilder.AddColumn<string>(
                name: "Faculty",
                table: "SuperPowers",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
