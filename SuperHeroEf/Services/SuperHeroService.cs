﻿using PostGradEF.DataAccess.Interfaces;
using PostGradEF.Models;
using PostGradEF.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradEF.Services
{
    public class SuperHeroService : ISuperHeroService
    {
        private readonly ISuperHeroRepository repository;

        public SuperHeroService(ISuperHeroRepository _repository)
        {
            repository = _repository;
        }

        public SuperHero Add(SuperHero hero)
        {
            return repository.Create(hero);
        }

        public SuperHero Edit(SuperHero hero)
        {
            return repository.Update(hero);
        }

        public List<SuperHero> Find()
        {
            return repository.Read();
        }

        public SuperHero FindById(int id)
        {
            return repository.Read(id);
        }

        public SuperHero FindByName(string name)
        {
            return repository.Read(name);
        }

        public bool Remove(int id)
        {
            return repository.Delete(id);
        }
    }
}
