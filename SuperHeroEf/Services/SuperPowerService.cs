﻿using PostGradEF.DataAccess.Interfaces;
using PostGradEF.Models;
using PostGradEF.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradEF.Services
{
    public class SuperPowerService : ISuperPowerService
    {
        private readonly ISuperPowerRepository repository;

        public SuperPowerService(ISuperPowerRepository _repository)
        {
            repository = _repository;
        }

        public SuperPower Add(SuperPower superPower)
        {
            throw new NotImplementedException();
        }

        public SuperPower Edit(SuperPower editedSuperPower)
        {
            throw new NotImplementedException();
        }

        public List<SuperPower> Find()
        {
            throw new NotImplementedException();
        }

        public SuperPower FindById(int id)
        {
            throw new NotImplementedException();
        }

        public SuperPower FindByName(string name)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
