﻿using PostGradEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradEF.Services.Interfaces
{
    public interface ISuperPowerService
    {
        public SuperPower Add(SuperPower superPower);
        public SuperPower Edit(SuperPower editedSuperPower);
        public List<SuperPower> Find();
        public SuperPower FindById(int id);
        public SuperPower FindByName(string name);
        public bool Remove(int id);
    }
}
