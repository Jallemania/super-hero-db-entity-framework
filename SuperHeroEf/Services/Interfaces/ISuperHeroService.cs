﻿using PostGradEF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGradEF.Services.Interfaces
{
    public interface ISuperHeroService
    {
        public SuperHero Add(SuperHero hero);
        public SuperHero Edit(SuperHero hero);
        public List<SuperHero> Find();
        public SuperHero FindById(int id);
        public SuperHero FindByName(string name);
        public bool Remove(int id);
    }
}
